package com.desa02.lab02

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        edtNacimiento.requestFocus()
        btnVerificar.setOnClickListener {

            if( edtNacimiento.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresar año de Nac.", Toast.LENGTH_SHORT).show()
                edtNacimiento.requestFocus()
                return@setOnClickListener
            }

            val anioNacimiento = edtNacimiento.text.toString().toInt()

            tvResultado.text = when(anioNacimiento){
                in 1930..1948 ->{
                    "Silent Genration."
                }
                in 1949..1968 ->{
                    "Baby Boom."
                }in 1969..1980->{
                    "Generación X."
                }
                in 1981..1993->{
                    "Generación Y."
                }
                in 1994..2010->{
                    "Generación Z."
                }
                else -> "Sin Generación."
            }

            tvPoblacion.text = when(anioNacimiento){
                in 1930..1948 ->{
                    "Población: 6,3000.000"
                }
                in 1949..1968 ->{
                    "Población: 12,200.000"
                }in 1969..1980->{
                    "Población: 9,300.000"
                }
                in 1981..1993->{
                    "Población: 7,200.000"
                }
                in 1994..2010->{
                    "Población: 7,800.000"
                }
                else -> "Sin Población."
            }

            imvRostro.setImageResource( when (anioNacimiento){
                in 1930..1948 ->{
                    R.drawable.ic_austeridad
                }
                in 1949..1968 ->{
                    R.drawable.ic_ambicion
                }in 1969..1980->{
                    R.drawable.ic_obsecion
                }
                in 1981..1993->{
                    R.drawable.ic_frustacion
                }
                in 1994..2010->{
                    R.drawable.ic_irreverencia
                }
                else -> R.drawable.shape_rectangle_imv
                }
            )
            edtNacimiento.requestFocus()
        }

        edtNacimiento.addTextChangedListener( object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                tvResultado.setText( "[Resultado]")
                tvPoblacion.setText( "[Resultado]")
                imvRostro.setImageResource(R.drawable.shape_rectangle_imv)
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        } )

    }
}
